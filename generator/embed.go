package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

const header = `// +build embed

// This file is machine generated. Do not edit manually!

package main

var embedded = [...]byte{`

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func main() {
	url := flag.String("url", "", "Source of file to download")
	file := flag.String("file", "embedded.go", "Destination file")

	flag.Parse()

	if *url == "" {
		log.Fatalln("missing url")
	}
	f, err := os.Create(*file)
	check(err)
	defer f.Close()

	out := bufio.NewWriter(f)

	resp, err := http.Get(*url)
	check(err)
	defer resp.Body.Close()

	r := bufio.NewReader(resp.Body)

	fmt.Fprintln(out, header)

	count := 0
	for {
		b, err := r.ReadByte()
		if err == io.EOF {
			break
		}
		check(err)
		if count > 0 {
			out.WriteByte(' ')
		} else {
			out.WriteByte('\t')
		}
		fmt.Fprintf(out, "0x%02x,", b)
		if count == 11 {
			fmt.Fprintln(out)
			count = 0
		} else {
			count++
		}
	}
	if count != 0 {
		fmt.Fprintln(out)
	}

	fmt.Fprintln(out, "}")

	out.Flush()
}
