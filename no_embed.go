// +build !embed

package main

const (
	// Path to the ding file.
	defaultDingFile = "/usr/share/trans/de-en"
)

func loadDing(
	filename string,
	howMany int, useLeft, justPassphrase, ascii bool,
) ([]string, bool, error) {
	return readDingFromFile(filename, howMany, useLeft, justPassphrase, ascii)
}
