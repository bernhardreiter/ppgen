// +build embed

package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
)

const (
	// Using from embedded
	defaultDingFile = ""
)

func loadDing(
	filename string,
	howMany int, useLeft, justPassphrase, ascii bool,
) ([]string, bool, error) {
	if filename == "" {
		e := bytes.NewReader(embedded[:])
		if r, err := gzip.NewReader(e); err != nil {
			return nil, false, err
		} else {
			if !justPassphrase {
				fmt.Printf("Reading entries from %s", filename)
			}
			return readDing(r, howMany, useLeft, justPassphrase, ascii)
		}
	}
	return readDingFromFile(filename, howMany, useLeft, justPassphrase, ascii)
}
